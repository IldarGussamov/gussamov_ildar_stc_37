package ru.inno.game.client.util;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import ru.inno.game.client.controllers.MainController;
import ru.inno.game.client.socket.SocketClient;

public class GameUtils {

    private static final int PLAYER_STEP = 10;
    private static final int DAMAGE = 5;

    private AnchorPane pane;
    private MainController controller;
    private SocketClient client;

    public void goLeft(Circle player) {
        player.setCenterX(player.getCenterX() - PLAYER_STEP);
    }

    public void goRight(Circle player) {
        player.setCenterX(player.getCenterX() + PLAYER_STEP);
    }

    // player - кто стреляет, isEnemy - false - мы стреляем во врага, isEnemy - true - враг в нас
    public Circle createBulletFor(Circle player, boolean isEnemy) {
        Circle bullet = new Circle();
        bullet.setRadius(5);
        pane.getChildren().add(bullet);
        bullet.setCenterX(player.getCenterX() + player.getLayoutX());
        bullet.setCenterY(player.getCenterY() + player.getLayoutY());
        bullet.setFill(Color.ORANGE);

        int value;

        if (isEnemy) {
            value = 1;
        } else {
            value = -1;
        }

        final Circle target;
        final Label targetHp;

        // определяем, куда стреляем
        if (!isEnemy) {
            target = controller.getEnemy();
            targetHp = controller.getHpEnemy();
        } else {
            target = controller.getPlayer();
            targetHp = controller.getHpPlayer();
        }

        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.005), animation -> {
            bullet.setCenterY(bullet.getCenterY() + value);
            // если пуля еще видна и произошло пересечение
            if (bullet.isVisible() && isIntersects(bullet, target)) {
                // уменьшить здоровье
                createDamage(targetHp);
                // скрыть пулю
                bullet.setVisible(false);
                if (!isEnemy) {
                    client.sendMessage("DAMAGE");
                }
            }

        }));

        timeline.setCycleCount(500);
        timeline.play();

        return bullet;
    }

    private boolean isIntersects(Circle bullet, Circle player) {
        return bullet.getBoundsInParent().intersects(player.getBoundsInParent());
    }

    private void createDamage(Label hpLabel) {
        int hpPlayer = Integer.parseInt(hpLabel.getText());
        hpLabel.setText(String.valueOf(hpPlayer - DAMAGE));
    }

    public void setClient(SocketClient client) {
        this.client = client;
    }

    public void setController(MainController controller) {
        this.controller = controller;
    }

    public void setPane(AnchorPane pane) {
        this.pane = pane;
    }
}
