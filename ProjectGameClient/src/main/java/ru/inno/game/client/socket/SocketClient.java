package ru.inno.game.client.socket;

import javafx.application.Platform;
import ru.inno.game.client.controllers.MainController;
import ru.inno.game.client.util.GameUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


// поток для получения сообщений с сервера
public class SocketClient extends Thread {
    // канал подключения
    private Socket socket;
    // стрим для отправления сообщений серверу
    private PrintWriter toServer;
    // стрим для получения сообщений от сервера
    private BufferedReader fromServer;

    private MainController controller;

    private GameUtils gameUtils;

    public SocketClient(MainController controller, String host, int port) {
        try {
            // создаем подключение к серверу
            socket = new Socket(host, port);
            // получаем стримы для чтения и записи
            toServer = new PrintWriter(socket.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.controller = controller;
            this.gameUtils = controller.getGameUtils();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    // ждем сообщений от сервера
    @Override
    public void run() {
        while (true) {
            String messageFromServer;
            try {
                // прочитали сообщение с сервера
                messageFromServer = fromServer.readLine();
                if (messageFromServer != null) {
                    switch (messageFromServer) {
                        case "left":
                            gameUtils.goLeft(controller.getEnemy());
                            break;
                        case "right":
                            gameUtils.goRight(controller.getEnemy());
                            break;
                        case "shot":
                            Platform.runLater(() -> gameUtils.createBulletFor(controller.getEnemy(), true));
                            break;
                    }
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    // отправляем сообщение серверу
    public void sendMessage(String message) {
        toServer.println(message);
    }

}
