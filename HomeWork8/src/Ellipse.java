public class Ellipse extends GeomFigure implements Sizeable, Moveable {
    int big_semi_axis; //большая полуось эллипса, используется в расчетах
    int small_semi_axis; //малая полуось эллипса, используется в расчетах

    public Ellipse(int big_semi_axis, int small_semi_axis) {
        this.big_semi_axis = big_semi_axis;
        this.small_semi_axis = small_semi_axis;
    }

    public double findSquare() {
        System.out.print("Площадь эллипса равна: ");
        return (PI * big_semi_axis * small_semi_axis);
    }

    public double findPerimeter() {
        System.out.print("Периметр эллипса равен: ");
        return (4 * ((PI * big_semi_axis * small_semi_axis) + (big_semi_axis - small_semi_axis)) / (big_semi_axis + small_semi_axis));
    }

    @Override
    public void changeSize() {
        this.big_semi_axis = big_semi_axis * coefficientOfSizeChange;
        this.small_semi_axis = small_semi_axis * coefficientOfSizeChange;
        System.out.println("Периметр эллипса теперь: " + findPerimeter());
        System.out.println("Площадь эллипса теперь: " + findSquare());
    }

    @Override
    public void changeCentre() {
        this.x_centre = x_centre + valueOfCentreChange;
        this.y_centre = y_centre + valueOfCentreChange;
        System.out.println("Координаты центра эллипса по оси Х изменились на следующие значения: " + x_centre);
        System.out.println("Координаты центра эллипса по оси Y изменились на следующие значения: " + y_centre);
    }
}
