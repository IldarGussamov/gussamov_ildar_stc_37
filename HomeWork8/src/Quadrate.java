public class Quadrate extends GeomFigure implements Moveable, Sizeable {
    int side_a;

    public Quadrate(int side_a) {
        this.side_a = side_a;
    }
    public double findSquare() {
        System.out.print("Площадь квадрата равна: ");
        return (side_a * side_a);
    }
    public double findPerimeter() {
        System.out.print("Периметр квадрата равен: ");
        return (side_a * 4);
    }
    @Override
    public void changeSize() {
        this.side_a = side_a * coefficientOfSizeChange;
        System.out.println("Периметр квадрата теперь: " + findPerimeter());
        System.out.println("Площадь квадрата теперь: " + findSquare());
    }

    @Override
    public void changeCentre() {
        this.x_centre = x_centre + valueOfCentreChange;
        this.y_centre = y_centre + valueOfCentreChange;
        System.out.println("Координаты центра квадрата по оси Х изменились на следующие значения: " + x_centre);
        System.out.println("Координаты центра квадрата по оси Y изменились на следующие значения: " + y_centre);
    }
}
