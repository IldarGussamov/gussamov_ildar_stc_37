public class Rectangle extends Quadrate implements Sizeable, Moveable {
    int side_b;

    public Rectangle(int side_a, int side_b) {
        super(side_a);
        this.side_b = side_b;
    }
    public double findSquare() {
        System.out.print("Площадь прямоугольгика равна: ");
       return(side_a * side_b);
    }
    public double findPerimeter() {
        System.out.print("Периметр прямоугольгика равен: ");
        return((side_a + side_b) * 2);
    }
    @Override
    public void changeSize() {
        this.side_a = side_a * coefficientOfSizeChange;
        this.side_b = side_b * coefficientOfSizeChange;
        System.out.println("Периметр прямоугольника теперь: " + findPerimeter());
        System.out.println("Площадь прямоугольника теперь: " + findSquare());
    }

    @Override
    public void changeCentre() {
        this.x_centre = x_centre + valueOfCentreChange;
        this.y_centre = y_centre + valueOfCentreChange;
        System.out.println("Координаты центра прямоугольника по оси Х изменились на следующие значения: " + x_centre);
        System.out.println("Координаты центра прямоугольника по оси Y изменились на следующие значения: " + y_centre);
    }
}

