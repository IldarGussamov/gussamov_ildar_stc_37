public abstract class GeomFigure {
    protected static final double PI = 3.14; //константа ПИ, используется в расчетах площади и периметра
    protected int x_centre = 0; //начальное положение центра координат по оси Х
    protected int y_centre = 0; //начальное положение центра координат по оси У
    int coefficientOfSizeChange = 3;
    int valueOfCentreChange = -4;

    //метод расчета площади фигуры
    public abstract double findSquare();

    //метод расчета периметра фигуры
    public abstract double findPerimeter();
}


