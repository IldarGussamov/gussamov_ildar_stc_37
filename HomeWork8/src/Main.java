import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //создаем фигуры - квадрат, прямоугольник, кругЭ эллипс
        Quadrate quadrate = new Quadrate(3);
        Rectangle rectangle = new Rectangle(3, 4);
        Circle circle = new Circle(3);//круг
        Ellipse ellipse = new Ellipse(4, 2);

        //создаем массив с фигурами
        GeomFigure[] figures = {quadrate, rectangle, ellipse, circle};
        for (int i = 0; i < figures.length; i++) {
            System.out.println(figures[i].findSquare()); //метод определения площади
            System.out.println(figures[i].findPerimeter()); //метод определения периметра
        }
        //меняем размер фигур
        quadrate.changeSize();
        rectangle.changeSize();
        ellipse.changeSize();
        circle.changeSize();
        //меняем координаты центра фигур
        quadrate.changeCentre();
        rectangle.changeCentre();
        circle.changeCentre();
        ellipse.changeCentre();

    }
}

