package ru.inno.game.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Player {
    private Long id;
    private String ip;
    private String name;
    private Integer points;
    private Integer maxWinsCount;
    private Integer maxLosesCount;

    public Player(String ip, String name, Integer points, Integer maxWinsCount, Integer maxLosesCount) {
        this.ip = ip;
        this.name = name;
        this.points = points;
        this.maxWinsCount = maxWinsCount;
        this.maxLosesCount = maxLosesCount;
    }


}
