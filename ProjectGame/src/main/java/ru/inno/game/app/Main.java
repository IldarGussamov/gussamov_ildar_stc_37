package ru.inno.game.app;
import ru.inno.game.dto.StatisticDto;
import ru.inno.game.repository.GamesRepository;
import ru.inno.game.repository.GamesRepositoryJdbcImpl;
import ru.inno.game.repository.PlayersRepository;
import ru.inno.game.repository.PlayersRepositoryJdbcImpl;
import ru.inno.game.repository.ShotsRepository;
import ru.inno.game.repository.ShotsRepositoryJdbcImpl;
import ru.inno.game.services.GameService;
import ru.inno.game.services.GameServiceImpl;
import ru.inno.game.utils.CustomDataSource;

import javax.sql.DataSource;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //время начала игры
        long startTimeMills = System.currentTimeMillis();

//        PlayersRepository playersRepository = new PlayersRepositoryMapImpl();

//        GamesRepository gamesRepository = new GamesRepositoryListImpl();
//        ShotsRepository shotsRepository = new ShotsRepositoryFileImpl("shots_db.txt", "shots_sequence.txt");
//        PlayersRepository playersRepository = new PlayersRepositoryFileImpl("player_db.txt", "player_sequence.txt");

        DataSource dataSource = new CustomDataSource("postgres", "infinite1312944", "jdbc:postgresql://localhost:5432/Project_game_db");

        //Базы данных SQL
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);

        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите имя первого игрока:");
        String first = scanner.nextLine();
        System.out.println("Введите имя второго игрока:");
        String second = scanner.nextLine();

        Random random = new Random();

        Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);
        String shooter = first;
        String target = second;
        int i = 0;
        //количесто выстрелов пока не закончится игра
        while (i < 6) {

            System.out.println(shooter + " делайте выстрел в " + target);
            scanner.nextLine();

            int success = random.nextInt(2);

            if (success == 0) {
                System.out.println("Успешно!");
                gameService.shot(gameId, shooter, target);
            } else {
                System.out.println("Промах!");
            }

            String temp = shooter;
            shooter = target;
            target = temp;
            i++;
        }
        // Необходимо вывести информацию
        StatisticDto statistic = gameService.finishGame(gameId, (System.currentTimeMillis() - startTimeMills) / 1000);
        System.out.println(statistic);

    }
}
