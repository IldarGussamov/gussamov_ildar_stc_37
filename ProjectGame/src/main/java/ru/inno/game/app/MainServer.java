package ru.inno.game.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.game.repository.*;
import ru.inno.game.server.GameServer;
import ru.inno.game.services.GameService;
import ru.inno.game.services.GameServiceImpl;


import javax.sql.DataSource;

public class MainServer {

    public static void main(String[] args) {
        //подключение с помощью Hikari ConnectionPool
        HikariConfig config = new HikariConfig();
        //данные для подключения
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/Project_game_db");
        config.setDriverClassName("org.postgresql.Driver");
        config.setUsername("postgres");
        config.setPassword("infinite1312944");
        //установить количество потоков подключений
        config.setMaximumPoolSize(20);
        //источник данных
        DataSource dataSource = new HikariDataSource(config);
        //создаем репозиторй для этого источника данных
        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource);
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);
        //сервис который использует созданные выше репозитории
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);
        //передали сервис обьекту-серверу для игры
        GameServer gameServer = new GameServer(gameService);
        gameServer.start(7777);
    }
}
