package ru.inno.game.dto;
import ru.inno.game.models.Game;
import ru.inno.game.repository.GamesRepository;
import ru.inno.game.repository.PlayersRepository;
import ru.inno.game.repository.ShotsRepository;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

// информация об игре
public class StatisticDto {
    private Long gameId;
    private String name;
    private Integer hit;
    private Integer points;
    private Long secondsGameTimeAmount;
    private Integer maxWinsCount;
    private Integer maxLosesCount;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHit() {
        return hit;
    }

    public void setHit(Integer hit) {
        this.hit = hit;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Long getSecondsGameTimeAmount() {
        return secondsGameTimeAmount;
    }

    public void setSecondsGameTimeAmount(Long secondsGameTimeAmount) {
        this.secondsGameTimeAmount = secondsGameTimeAmount;
    }

    public Integer getMaxWinsCount() {
        return maxWinsCount;
    }

    public void setMaxWinsCount(Integer maxWinsCount) {
        this.maxWinsCount = maxWinsCount;
    }

    public Integer getMaxLosesCount() {
        return maxLosesCount;
    }

    public void setMaxLosesCount(Integer maxLosesCount) {
        this.maxLosesCount = maxLosesCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatisticDto that = (StatisticDto) o;
        return Objects.equals(gameId, that.gameId) && Objects.equals(name, that.name) && Objects.equals(hit, that.hit) && Objects.equals(points, that.points) && Objects.equals(secondsGameTimeAmount, that.secondsGameTimeAmount) && Objects.equals(maxWinsCount, that.maxWinsCount) && Objects.equals(maxLosesCount, that.maxLosesCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gameId, name, hit, points, secondsGameTimeAmount, maxWinsCount, maxLosesCount);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(" ");
        return sb.toString();
    }
}