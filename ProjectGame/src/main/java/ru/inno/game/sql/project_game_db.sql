CREATE TABLE player
(
    id                bigserial PRIMARY KEY,
    player_ip_address varchar(100),
    player_name       varchar(100),
    max_point         integer,
    win_count         integer,
    lose_count        integer
);

CREATE TABLE game
(
    id                    bigserial PRIMARY KEY,
    date_of_game          varchar(100),
    player_one            bigint,
    player_two            bigint,
    player_one_shot_count integer,
    player_two_shot_count integer,
    time_of_game          bigint,
    FOREIGN KEY (player_one) REFERENCES player (id),
    FOREIGN KEY (player_two) REFERENCES player (id)
);

CREATE TABLE shot
(
    id                   bigserial PRIMARY KEY,
    time_to_shoot        varchar(100),
    game                 bigint,
    player_who_shoot     bigint,
    player_who_target    bigint,
    FOREIGN KEY (game) REFERENCES game (id),
    FOREIGN KEY (player_who_shoot) REFERENCES player (id),
    FOREIGN KEY (player_who_target) REFERENCES player (id)
);