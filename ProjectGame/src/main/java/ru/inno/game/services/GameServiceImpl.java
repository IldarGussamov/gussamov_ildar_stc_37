package ru.inno.game.services;

import ru.inno.game.dto.StatisticDto;
import ru.inno.game.models.Game;
import ru.inno.game.models.Player;
import ru.inno.game.models.Shot;
import ru.inno.game.repository.GamesRepository;
import ru.inno.game.repository.PlayersRepository;
import ru.inno.game.repository.ShotsRepository;

import java.time.LocalDateTime;

// бизнес-логика
public class GameServiceImpl implements GameService {

    private final PlayersRepository playersRepository;

    private final GamesRepository gamesRepository;

    private final ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        // получили информацию об обоих игроках
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);
        // создали игру
        Game game = Game.builder()
                .dateTime(LocalDateTime.now())
                .playerFirst(first)
                .playerSecond(second)
                .playerFirstShotsCount(0)
                .playerSecondShotsCount(0)
                .secondsGameTimeAmount(0L)
                .build();
        // сохранили игру в репозитории
        gamesRepository.save(game);
        return game.getId();
    }

    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        // если нет первого игрока под таким именем
        if (player == null) {
            // создаем игрока
            player = Player.builder()
                    .name(nickname)
                    .ip(ip)
                    .points(0)
                    .maxWinsCount(0)
                    .maxLosesCount(0)
                    .build();
            // сохраняем его в репозитории
            playersRepository.save(player);
        } else {
            // если такой игрок был -> обновляем у него IP-адрес
            player.setIp(ip);
            playersRepository.update(player);
        }

        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        // получаем того, кто стрелял из репозитория
        Player shooter = playersRepository.findByNickname(shooterNickname);
        // получаем того, в кого стреляли из репозитория
        Player target = playersRepository.findByNickname(targetNickname);
        // получаем игру
        Game game = gamesRepository.findById(gameId);
        // создаем выстрел

        Shot shot = Shot.builder()
                .dateTime(LocalDateTime.now())
                .game(game)
                .shooter(shooter)
                .target(target)
                .build();

        // увеличиваем очки у стреляющего
        shooter.setPoints(shooter.getPoints() + 1);
        // если стрелявший - первый игрок
        if (game.getPlayerFirst().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле в игре
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }
        // если стрелявший - второй игрок
        if (game.getPlayerSecond().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле в игре
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }
        // обновляем данные по стреляющему
        playersRepository.update(shooter);
        // обновляем данные по игре
        gamesRepository.update(game);
        // сохраняем выстрел
        shotsRepository.save(shot);
    }

    @Override
    public StatisticDto finishGame(Long gameId, long second) {
        StatisticDto statisticDto = new StatisticDto();
        final Game game = gamesRepository.findById(gameId);

        //имена игроков
        final String playerNameOne = game.getPlayerFirst().getName();
        final String playerNameTwo = game.getPlayerSecond().getName();
        //попадания
        final int hitPlayerOne = game.getPlayerFirstShotsCount();
        final int hitPlayerTwo = game.getPlayerSecondShotsCount();
        //количество очков
        final int maxPointPlayerOne = game.getPlayerFirst().getPoints();
        final int maxPointPlayerTwo = game.getPlayerSecond().getPoints();
        //победы
        int maxWinPlayerOne = game.getPlayerFirst().getMaxWinsCount();
        int maxWinPlayerTwo = game.getPlayerSecond().getMaxWinsCount();
        //поражения
        int loseCountPlayerOne = game.getPlayerFirst().getMaxLosesCount();
        int loseCountPlayerTwo = game.getPlayerSecond().getMaxLosesCount();

        final Player playerFirst = playersRepository.findByNickname(playerNameOne);
        final Player playerSecond = playersRepository.findByNickname(playerNameTwo);

        //устанавливаем время проведенное в игре (реализация в GameServer)
        game.setSecondsGameTimeAmount(second);

        //выиграл первый игрок
        if (hitPlayerOne > hitPlayerTwo) {
            //добавляем к победам еще одну для игрока
            playerFirst.setMaxWinsCount(playerFirst.getMaxWinsCount() + 1);
            //добавляем к поражениям еще одно для противника
            playerSecond.setMaxLosesCount(playerSecond.getMaxLosesCount() + 1);
            //пишем победителя
            System.out.println("Победитель: " + playerNameOne + ";");
            //информация о первом игроке
            System.out.println("Первый игрок: " + playerNameOne +
                    "; hit = " + hitPlayerOne +
                    "; Max Point = " + maxPointPlayerOne +
                    "; Win Count = " + playerFirst.getMaxWinsCount() +
                    "; Lose Count = " + loseCountPlayerOne +
                    ";");
            //о втором игроке
            System.out.println("Второй игрок: " + playerNameTwo +
                    "; hit = " + hitPlayerTwo +
                    "; Max Point = " + maxPointPlayerTwo +
                    "; Win Count = " + maxWinPlayerTwo +
                    "; Lose Count = " + playerSecond.getMaxLosesCount() +
                    ";");
        }
        //если выиграл второй игрок
        else if (hitPlayerOne < hitPlayerTwo) {

            playerFirst.setMaxLosesCount(playerFirst.getMaxLosesCount() + 1);
            playerSecond.setMaxWinsCount(playerSecond.getMaxWinsCount() + 1);

            System.out.println("Победитель: " + playerNameTwo + ";");

            System.out.println("Первый игрок: " + playerNameOne +
                    "; hit = " + hitPlayerOne +
                    "; Max Point = " + maxPointPlayerOne +
                    "; Win Count = " + maxWinPlayerOne +
                    "; Lose Count = " + playerFirst.getMaxLosesCount() +
                    ";");
            System.out.println("Второй игрок: " + playerNameTwo +
                    "; hit = " + hitPlayerTwo +
                    "; Max Point = " + maxPointPlayerTwo +
                    "; Win Count = " + playerSecond.getMaxWinsCount() +
                    "; Lose Count = " + loseCountPlayerTwo +
                    ";");
            //если ничья
        } else {
            System.out.println("Ничья");

            System.out.println("Первый игрок: " + playerNameOne +
                    "; hit = " + hitPlayerOne +
                    "; Max Point = " + maxPointPlayerOne +
                    "; Win Count = " + playerFirst.getMaxWinsCount() +
                    "; Lose Count = " +  playerFirst.getMaxLosesCount() +
                    ";");
            System.out.println("Второй игрок: " + playerNameTwo +
                    "; hit = " + hitPlayerTwo +
                    "; Max Point = " + maxPointPlayerTwo +
                    "; Win Count = " + playerSecond.getMaxWinsCount() +
                    "; Lose Count = " + playerSecond.getMaxLosesCount() +
                    ";");
        }

        //сохраняем в базы данных изменения
        playersRepository.update(playerFirst);
        playersRepository.update(playerSecond);
        gamesRepository.update(game);
        //время затраченное на игру
        System.out.println("Время игры: " + game.getSecondsGameTimeAmount() + " сек.");

        return statisticDto;
    }

}
