public class Main {
    public static void main(String[] args) {

        //реализация лямбда выражения NumbersProcess
        NumbersAndStringProcessor numbersAndStringProcessor = new NumbersAndStringProcessor() {
            protected int process(int number) {
                return reverseNumber(number);
            }

            protected String process(String process) {
                return reverseString(process);
            }
        };
        NumbersAndStringProcessor numbersAndStringProcessor1 = new NumbersAndStringProcessor() {
            protected int process(int number) {
                return removeZeros(number);
            }

            protected String process(String process) {
                return removeAllNumbers(process);
            }
        };
        NumbersAndStringProcessor numbersAndStringProcessor2 = new NumbersAndStringProcessor() {
            protected int process(int number) {
                return evenNumber(number);
            }

            protected String process(String process) {
                return upperCaseText(process);
            }
        };
        //разворот строки и числа
        numbersAndStringProcessor.process(25);
        numbersAndStringProcessor.process("sometext");
        //удаление нулей
        numbersAndStringProcessor1.process(50206);
        numbersAndStringProcessor1.process("some0text0with0zeros0");
        //число к ближайшему четному
        numbersAndStringProcessor2.process(605);
        numbersAndStringProcessor2.process("Some Text with upper case");

        //Выводим информацию на экран
        System.out.println("Разворот числа и строки: " + numbersAndStringProcessor);
        System.out.println("Удаление нулей из чисел и цифр из строк: " + numbersAndStringProcessor1);
        System.out.println("Число ближайшее к четному и превращение строчных букв в прописные: " + numbersAndStringProcessor2);
    }


    //строчные буквы в прописные
    private static String upperCaseText(String process) {
        return process.toUpperCase();
    }

    //число ближайшее к четному
    private static int evenNumber(int number) {
        if (number % 2 == 0) {
            return number;
        } else {
            return number - 1;
        }
    }

    //удаление чисел из строки
    private static String removeAllNumbers(String process) {
        return process.replaceAll("\\d", "");
    }

    //удаление нулей из числа
    private static int removeZeros(int number) {
        if (number != 0) {
            return Integer.parseInt(Integer.toString(number).replace("0", ""));
        }
        return number;
    }

    //разворот числа
    private static int reverseNumber(int number) {
        int numberReversed = 0;

        while (number > 0) {
            numberReversed = numberReversed * 10 + number % 10;
            number = number / 10;
        }
        return numberReversed;
    }

    //разворот строки
    private static String reverseString(String process) {
        if (process.length() <= 1) {
            return process;
        }
        return reverseString(process.substring(1)) + process.charAt(0);
    }
}
