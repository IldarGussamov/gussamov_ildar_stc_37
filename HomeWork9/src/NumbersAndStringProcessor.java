public class NumbersAndStringProcessor {
    private static final int MAX_PROCESSED_NUMBERS_COUNT = 5;
    private int processedNumbers[];
    private String processedString[];
    private int processedNumbersCount;
    private int processedStringCount;

    public NumbersAndStringProcessor() {
        this.processedNumbers = new int[MAX_PROCESSED_NUMBERS_COUNT];
        this.processedString = new String[MAX_PROCESSED_NUMBERS_COUNT];
    }

    public void numberProcess(int number) {
        if (processedNumbersCount < MAX_PROCESSED_NUMBERS_COUNT) {
            int processedNumber = process(number);
            saveInputNumber(processedNumber);
        } else {
            System.err.println("Кончилось место для обработки чисел");
        }
    }

    public void stringProcess(String process) {
        if (processedStringCount < MAX_PROCESSED_NUMBERS_COUNT) {
            String processedString = process(process);
            saveInputString(processedString);
        } else {
            System.err.println("Кончилось место для обработки строк");
        }
    }

    private void saveInputNumber(int number) {
        processedNumbers[processedNumbersCount++] = number;
    }

    private void saveInputString(String process) {
        processedString[processedStringCount++] = process;
    }

    public int showProcessedNumber() {
        int a = 0;
        for (int i = 0; i < processedNumbersCount; i++) {
            a = processedNumbers[i];
        }
        return a;
    }

    public String showProcessedString() {
        String b = " ";
        for (int i = 0; i < processedStringCount; i++) {
            b = processedString[i];
        }
        return b;
    }
    public String toString() {
        return "Результат по цифрам:" + showProcessedNumber() + "\n" + "Результат по строкам: " + "\n";
    }
    int process(int number) {
        return number;
    }
    String process(String process) {
        return process;
    }
}
