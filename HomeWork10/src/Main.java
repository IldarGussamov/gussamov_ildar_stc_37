public class Main {

    public static void main(String[] args) {
        // создал список
        InnoList list = new InnoLinkedList();

        list.add(7);
        list.add(8);
        list.add(10);
        list.add(12);
        list.add(15);
        list.add(20);
        list.add(-77);
        list.add(100);

        //Исходный связанный список
        printList((InnoList) list);

        //Получаем значение элемента по индексу
        System.out.println(list.get(0));

        //замена элемента по индексу
        list.insert(1, 80);
        printList(list);

        //добавляем элемент в начало списка
        list.addToBegin(200);
        printList(list);

        //удаляем элемент по индексу
        list.removeByIndex(5);
        printList(list);

        //удаляем элемент
        list.remove(12);
        printList(list);

        //поиск элемента в коллекции
        System.out.println(list.contains(20));

    }

    //вывод значений списка
    private static void printList(InnoList list) {
        InnoIterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
    }

}
