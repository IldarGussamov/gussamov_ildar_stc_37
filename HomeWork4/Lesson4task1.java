import java.util.Scanner;
class Lesson4task1 {
	public static double recursion(double n){
	if (n == 1) {
        return 1;
	}		
	else if ((n > 1) && (n < 2)) {
        return 0;
	}
	else {
        return recursion(n / 2);
	}
	}	
	public static void main(String []args){
	System.out.println ("Enter number: ");
	Scanner scanner = new Scanner(System.in);
	double n = scanner.nextDouble();
	
	if (recursion(n) == 1) {
            System.out.println("This number is power of 2");
        } else {
            System.out.println("This number is not power of 2");
        }
	}
}
	
	