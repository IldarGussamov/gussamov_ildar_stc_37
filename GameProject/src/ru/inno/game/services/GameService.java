package ru.inno.game.services;

public interface GameService {

    Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname); //метод начала игры
}
