package ru.inno.game.models;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

public class Shot {
    private Long id;
    private LocalDateTime dateTime;
    private Game game;
    private Player who;
    private Player whom;

    public Shot(LocalDateTime dateTime, Game game, Player who, Player whom) {
        this.dateTime = dateTime;
        this.game = game;
        this.who = who;
        this.whom = whom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Player getWho() {
        return who;
    }

    public void setWho(Player who) {
        this.who = who;
    }

    public Player getWhom() {
        return whom;
    }

    public void setWhom(Player whom) {
        this.whom = whom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shot shot = (Shot) o;
        return Objects.equals(dateTime, shot.dateTime) &&
                Objects.equals(game, shot.game) &&
                Objects.equals(who, shot.who) &&
                Objects.equals(whom, shot.whom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateTime, game, who, whom);
    }
}
