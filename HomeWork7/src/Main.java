public class Main {

    public static void main(String[] args) {
        User user1 = new User.Builder()
                .firstName("Marsel")
                .lastName("Sidikov")
                .age(26)
                .isWorker(true)
                .build();


        User user2 = new User.Builder()
                .firstName("Firstname_2")
                .lastName("Lastname_2")
                .age(20)
                .build();

        User user3 = new User.Builder()
                .lastName("Lastname_3")
                .age(8)
                .build();

        User[] users = {user1, user2, user3};
        for (int i = 0; i < users.length; i++) {
            System.out.println(users[i]);
        }
    }
}

