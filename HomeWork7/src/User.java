public class User {
    private String firstName = "Unknown";
    private String lastName = "Unknown";
    private int age;
    private boolean isWorker;



    protected static class Builder {
        private User user;
        public Builder() {
            user = new User();
        }

        public Builder firstName(String firstName) {
            user.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            user.lastName = lastName;
            return this;
        }

        public Builder age(int age) {
            user.age = age;
            return this;
        }

        public Builder isWorker(boolean isWorker) {
            user.isWorker = isWorker;
            return this;
        }

        public User build() {
            return user;
        }
    }


    public String toString() {
        return "New user info: {" +
                "firstName = '" + firstName + '\'' +
                "; lastName = '" + lastName + '\'' +
                "; age = " + age +
                "; isWorker = " + isWorker +
                '}';
    }

}
