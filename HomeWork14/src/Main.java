public class Main {

    public static void main(String[] args) {
        CarRepository carRepository = new CarRepositoryFileImpl("car_db.txt");

        //исходный список авто
        System.out.println(carRepository.carsToLines());
        System.out.println();

        System.out.println("номера авто черного цвета или имеющих нулевой пробег: ");
        System.out.println(carRepository.findBlackCar("Black", 0L));
        System.out.println();

        System.out.println("количество уникальных моделей авто стоимостью от 700000 до 800000: ");
        System.out.println(carRepository.findUniqueModelsCarByPrice(700000L, 800000L));
        System.out.println();

        System.out.println("цвет авто с минимальной стоимостью: ");
        System.out.println(carRepository.colorCarByMinPrice());
        System.out.println();

    }

}
